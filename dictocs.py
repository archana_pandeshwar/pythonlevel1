def get_cs() :
    return input("Enter the string")

def cs_to_dict(cs) :
    d = {}
    l = []
    l = cs.split(';')
    for i in l :
        k,v = i.split('=')
        d[k] = v
    return d



def dict_to_cs(v) :
    cs = ""
    for k,p in v.items() :
        cs = cs + k + "=" + p + ";"
    cs = cs.rstrip(cs[-1])
    return cs

def output(d,l) :
    print(d,l)

cs = get_cs()
v = cs_to_dict(cs)
j = dict_to_cs(v)
output(v,j)