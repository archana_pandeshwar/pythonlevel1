def get_input() :
    return float(input("Enter the number whose sine value needs to be found"))
def compute_sine(x) :
    y = 0
    term = x
    fact,i,k = 1,1,2
    error = 0.00000000001
    while (term/10)>error :
        y = y + (term/fact)
        term = term*x*x
        i +=2
        fact=-1*(fact*i*(i-1))
    return y
def output(x,y) :
    print("The sine of angle in radians %f is %10.30f"%(x,y))

x = get_input()
y = compute_sine(x)
output(x,y)
