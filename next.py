import math
def get_input() :
    return float(input("Enter the number"))
def compute_nextterm(x) :
    sum = 0
    term = 1
    for i in range(1,5) :
        sum = sum + term
        term = term*x
    return term
def output(term) :
    print("The sum of the series is (%f)"%(term))

x = get_input()
y = compute_nextterm(x)
output(y)