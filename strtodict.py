def get_cs() :
    return input("Enter the string")

def cs_to_dict(cs) :
    d = {}
    l = []
    l = cs.split(';')
    for i in l :
        k,v = i.split('=')
        d[k] = v
    return d

def output(d) :
    print(d)

cs = get_cs()
v = cs_to_dict(cs)
output(v)