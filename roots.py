import math
import cmath
def get_input() :
    return float(input("Enter the value of a")),float(input("Enter the value of b")),float(input("Enter the value of c"))
def det(a,b,c) :
    d = b**2 - 4*a*c
    return d
def compute_roots(a,b,c) :
    d = det(a,b,c)
    root1 = (-b+cmath.sqrt(d))/(2*a)
    root2 = (-b-cmath.sqrt(d))/(2*a)
    return root1,root2
def output(root1,root2) :
    print("The roots of the equation are {}+{}j and {}+{}j".format(root1.real,root1.imag,root2.real,root2.imag))

a,b,c = get_input()
d = det(a,b,c)
root1,root2 = compute_roots(a,b,c)
output(root1,root2)
